# vlan
Vlan package for NSO with a Yang validation for the interfaces.

## Configuration

This packages has been tested on ncs 4.6.X

Make sure you add JUnit 4 and Mockito to you NSO jars folder.

## Example

admin@ncs(config)# services vlan test vlan-id 5
admin@ncs(config-vlan-test)# commit


